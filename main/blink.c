/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "app_camera.h"


/* Can run 'make menuconfig' to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/
#define BLINK_GPIO CONFIG_BLINK_GPIO

const char * TAG = "Blink";

void task_process (void *arg)
{
    size_t frame_num = 0;
    camera_fb_t *fb = NULL;

    ESP_LOGI(TAG, "Starting camera process\n");

    do
    {
        int64_t start_time = esp_timer_get_time();
        /* 2. Get one image with camera */
        fb = esp_camera_fb_get();
        if (!fb)
        {
            ESP_LOGE(TAG, "Camera capture failed");
            continue;
        }
        int64_t fb_get_time = esp_timer_get_time();
        ESP_LOGI(TAG, "Get one frame in %lld ms.", (fb_get_time - start_time) / 1000);

        /* 3. Allocate image matrix to store RGB data */
        //image_matrix = dl_matrix3du_alloc(1, fb->width, fb->height, 3);

//        /* 4. Transform image to RGB */
//        uint32_t res = fmt2rgb888(fb->buf, fb->len, fb->format, image_matrix->item);
//        if (true != res)
//        {
//            ESP_LOGE(TAG, "fmt2rgb888 failed, fb: %d", fb->len);
//            dl_matrix3du_free(image_matrix);
//            continue;
//        }
//
//        esp_camera_fb_return(fb);
//
//        /* 5. Do face detection */
//        dl_matrix3du_free(image_matrix);

    } while(1);
}/*}}}*/

void app_main()
{
    /* Configure the IOMUX register for pad BLINK_GPIO (some pads are
       muxed to GPIO on reset already, but some default to other
       functions and need to be switched to GPIO. Consult the
       Technical Reference for a list of pads and their default
       functions.)
    */
    app_camera_init();
    xTaskCreatePinnedToCore(task_process, "camera", 4 * 1024, NULL, 5, NULL, 1);

//    camera_fb_t *fb = NULL;
//
//    ESP_LOGI(TAG, "Starting camera process\n");
//
//    do {
//        int64_t start_time = esp_timer_get_time();
//        /* 2. Get one image with camera */
//        fb = esp_camera_fb_get();
//        if (!fb) {
//            ESP_LOGE(TAG, "Camera capture failed");
//            continue;
//        }
//        int64_t fb_get_time = esp_timer_get_time();
//        ESP_LOGI(TAG, "Get one frame in %lld ms.", (fb_get_time - start_time) / 1000);
//    }while(1);

//    gpio_pad_select_gpio(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
//    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
    while(1) {
        /* Blink off (output low) */
//	printf("Turning off the LED\n");
//        gpio_set_level(BLINK_GPIO, 0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        /* Blink on (output high) */
//	printf("Turning on the LED\n");
//        gpio_set_level(BLINK_GPIO, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
